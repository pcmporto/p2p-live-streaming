Vue.component('watch', {
    props: ['room', 'stream', 'messages', 'send_message'],
    template: 
`<div class="center">
    <div class="col s12 m4 offset-m4" >
        <div class="card">
            <nav>
                <div class="nav-wrapper teal">
                    <div class="row" >
                        <div class="col s6" >
                            {{room.name}}, {{room.owner_name}}
                        </div>
                        <div class="col s6" >
                            <ul class="right">
                                <li><a href="#!" @click="$emit('leave-room')" ><i class="material-icons">exit_to_app</i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
            <div class="card-content">
                <div class="row">
                    <div class="col s6">
                        <div class="videoWrapper" >
                            <video v-if="stream" class="responsive-video" autoplay :srcObject.prop="stream" ></video>
                            <loading v-else></loading>
                        </div>
                    </div>
                    <div class="col s6">
                    <chat v-bind:messages="messages" @send_message="send_message"></chat>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>`
  })