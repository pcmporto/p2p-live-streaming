Vue.component('login-form', {
    props: ['stream'],
    data : function () {
        return  {
            input_name: ''
        }
    },
    template: 
`<div class="row" >
    <div class="col s12 m4 offset-m4" >
        <div class="card">
            <div class="card-action teal lighten-1 white-text" >
                <h3 class="card-title" >p2p live app</h3>
            </div>
            <div class="card-content">
                    <div class="row">
                        <div class="col s6">
                            <div class="row input-field">
                                <input id="inputName" type="text" v-model="input_name" required />
                                <label for="inputRoom">Enter your name:</label>
                            </div><br>
                            <button  @click="$emit('click-login', input_name)" class="btn-large waves-effect waves-dark">Login</button>
                        </div>
                        <div class="col s6">
                            <div v-if="stream" class="videoWrapper" >
                                <video class="responsive-video" :srcObject.prop="stream" autoplay muted></video>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>`
  })