Vue.component('chat-message', {
    props : ['msg'],
    template: `<div><p>{{msg.name}}</p><p>{{msg.message}}</p></div>`
})

Vue.component('chat', {
    props: ['messages'],
    data : function () {
        return  {
            input_message: ''
        }
    },
    methods: {
        send_message() {
            if (this.input_message != '') {
                this.$emit('send_message', this.input_message)
                this.input_message = ''
            }
        }
    },
    template: 
`<div class="col s3">
    <div class="row">
        <div class="input-field">
            <input id="inputChatMessage" type="text" v-model="input_message">
            <label for="inputChatMessage">Message:</label>
        </div><br>
        <button class="btn-large waves-effect waves-dark" @click="send_message"  >
            <i class="material-icons">send</i>
        </button>
    </div>
    <div class="row">
        <chat-message v-for="(msg, idx) in messages" v-bind:msg="msg" v-bind:key="idx" ></chat-message>
    </div>
</div>`
})