Vue.component('create-room', {
    data : function () {
        return  {
            input_room_name: ''
        }
    },
    template: 
`<div class="row" >
    <div class="col s12 m4 offset-m4" >
        <div class="card">
            <div class="card-action teal lighten-1 white-text" >
                <h3 class="card-title" >Create a room</h3>
            </div>
            <div class="card-content">
                    <div class="row">
                        <div class="col s12">
                            <div class="row input-field">
                                <input id="inputRoom" type="text" v-model="input_room_name" required />
                                <label for="inputRoom">Enter room name:</label>
                            </div><br>
                            <button  @click="$emit('create-room', input_room_name)" class="btn-large waves-effect waves-dark">Create</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>`
  })