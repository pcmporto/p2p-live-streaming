Vue.component('connect-to-room', {
    props: ['rooms'],
    template: 
`<div class="row">
    <div class="col s12 m4 offset-m4" >
        <div class="card">
            <div class="card-action teal lighten-1 white-text" >
                <h3 class="card-title" >Choose a room to connect</h3>
            </div>
            <div class="card-content" v-if="rooms.length > 0" >
                <div class="collection">
                    <a href="#!" class="collection-item" v-for="room in rooms" @click="$emit('room-selected', room)" >{{room}}</a>
                </div>
            </div>
            <div v-else>
                <p>There is no room available to join</p>
            </div>
        </div>
    </div>
</div>`
  })