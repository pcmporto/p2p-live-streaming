const CREATE_ROOM = 'create-room'
const UPDATE_ROOM_LIST = "update-room-list"
const JOIN_ROOM = "join-room"
const ROOM_CLOSED = "room-closed"
const USER_LEAVE_ROOM = 'user-leave-room'
const LEAVE_ROOM = 'leave-room'

const EVENT_CALL = 'call'
const EVENT_OFFER = 'offer'
const EVENT_ANSWER = 'answer'
const EVENT_CANDIDATE = 'candidate'
const EVENT_DISCONNECT = 'disconnect'

var app = new Vue({
    el: '#app',
    data: {
        current_room : null,
        allow_streaming: false,
        socket: null,
        state: 'loading',
        user_name: '',
        stream: null,
        remote_stream: null,
        peer: null,
        messages: [],
        rooms : [],
        peers : {},
        peers_count : 0
    },
    methods: {
        write_message(message) {
            if(this.messages.length >= 50) {
                this.messages.shift()
            }
            this.messages.push(JSON.parse(message))
        },
        broadcast_message(message) {
            for (const [key, peer] of Object.entries(this.peers)) {
                peer.dc.send(message)
            }
            this.write_message(message)
        },
        send_message(message) {
            if(this.peer && this.peer.dc) {
                this.peer.dc.send(JSON.stringify({
                    name: this.user_name,
                    message
                }))
            }
        },
        send_message_streamer(message) {
            this.broadcast_message(JSON.stringify({
                name: this.user_name,
                message
            }))
        },
        leave_room() {
            var v = this

            this.socket.emit(LEAVE_ROOM, this.current_room)

            if (v.state === 'watch') {
                v.peer.close()
                v.peer.onicecandidate = null
                v.peer.ontrack = null
            } else if (v.state === 'streaming') {
                for (var key in v.peers) {
                    v.peers[key].close()
                    v.peers[key].onicecandidate = null
                    v.peers[key].ontrack = null
                }
            }
            v.current_room = null
            v.peer = null
            v.remote_stream = null
            v.messages = []
            v.peers = {}
            v.peers_count = 0
            v.state = 'connected'
        },
        createPeer (streamer, user) {
            var v = this
            const rtcConfiguration = {
                iceServers: [{
                  urls: 'stun:stun.l.google.com:19302'
                }]
            }
            var pc = new RTCPeerConnection(rtcConfiguration)
            pc.onicecandidate = function (event) {
                if(!event.candidate) {
                    return
                }
                
                v.socket.emit(EVENT_CANDIDATE, {
                    user,
                    candidate: event.candidate
                })
            }
            
            if (streamer) {
                for (const track of v.stream.getTracks()) {
                    pc.addTrack(track, v.stream);
                }
                pc.dc = pc.createDataChannel('chat')

                pc.dc.onmessage = function(message) {
                    if(streamer) {
                        v.broadcast_message(message.data)
                    }
                }
            } else {
                pc.ontrack = function (event) {
                    if (v.remote_stream) {
                        return
                    }
                    v.remote_stream = event.streams[0]
                }

                pc.ondatachannel = function (event) {
                    pc.dc = event.channel
                    pc.dc.onmessage = function(message) {
                        v.write_message(message.data)
                    }
                }
            }            
            return pc
        },
        request_devices_perms() {
            var v = this
            var media = navigator.mediaDevices
            if (media) {
                media.getUserMedia({ video: {
                    height: 480,
                    width: 640
                }, audio: true })
                .then(function (stream) {
                    v.stream  = stream
                    v.state = 'login'
                    v.allow_streaming = true
                }).catch(function (err) {
                    v.stream = null
                    v.state = 'login'
                    v.allow_streaming = false
                    console.log(err)
                })
            } else {
                v.stream = null
                v.state = 'login'
                v.allow_streaming = false
            }
        },
        connect(name) {
            var v = this

            if (name.trim() === '') {
                M.toast({
                    html : '<span>Please enter a name!</span>'
                })
            } else {
                v.user_name = name
                v.state = 'connected'
                v.socket = io({
                    reconnection: false
                })

                v.socket.on(EVENT_CALL, function (data) {
                    var pc = v.createPeer(true, data.user)
                    v.peers[data.user] = pc
                    pc.createOffer().then(function (offer) {
                        pc.setLocalDescription(offer).then(function () {
                            v.socket.emit(EVENT_OFFER, {
                                user: data.user,
                                offer: offer
                            })
                        })
                    })
                    v.peers_count++
                })

                v.socket.on(EVENT_OFFER, function (data) {
                    if(v.peer == null) {
                        v.peer = v.createPeer(false, data.user)
                    }
                    v.peer.setRemoteDescription(data.offer).then(function () {
                        v.peer.createAnswer().then(function(answer) {
                            v.peer.setLocalDescription(answer).then(function() {
                                v.socket.emit(EVENT_ANSWER, {
                                    user: data.user,
                                    answer: answer
                                })
                            })
                        })
                    })
                })

                v.socket.on(EVENT_ANSWER,  function (data) {
                    if (data.user in v.peers) {
                        v.peers[data.user].setRemoteDescription(data.answer)
                    }
                })

                v.socket.on(EVENT_CANDIDATE,  function (data) {
                    if (v.state === 'watch') {
                        if(v.peer == null) {
                            v.peer = v.createPeer(false, data.user)
                        }
                        v.peer.addIceCandidate(data.candidate)
                    } else if (v.state === 'streaming') {
                        var peer
                        if (data.user in v.peers) {
                            var peer = v.peers[data.user]
                            peer.addIceCandidate(data.candidate)
                        }
                    }
                })

                v.socket.on(UPDATE_ROOM_LIST, function (rooms) {
                    v.rooms = rooms
                })

                v.socket.on(CREATE_ROOM, function (status) {
                    if(status.created) {
                        v.state = 'streaming'
                        v.peers = {}
                        v.current_room = status.room
                    } else {
                        v.state = 'connected'
                        var message =  status.error ? status.error : 'Não foi possível criar a sala'
                        M.toast({
                            html : '<span>' + message + '</span>'
                        })
                    }
                })

                v.socket.on(JOIN_ROOM, function (status) {
                    if (status.join) {
                        v.state = "watch"
                        v.current_room = status.room
                    } else {
                        v.state = "connected"
                        M.toast({
                            html : '<span>Não foi possível entrar na sala</span>'
                        })
                    }
                })

                v.socket.on(ROOM_CLOSED, function (data) {
                    if (v.current_room != null && v.current_room.name == data.name) {
                        v.state = 'connected'
                        v.current_room = null
                        M.toast({
                            html : '<span>Room closed.</span>'
                        })
                    }
                })

                v.socket.on(USER_LEAVE_ROOM, function (data) {
                    if (data.user in v.peers) {
                        var pc = v.peers[data.user]
                        pc.close()
                        pc.onicecandidate = null
                        pc.ontrack = null

                        delete v.peers[data.user]
                        v.peers_count--
                    }
                })

                v.socket.on(EVENT_DISCONNECT, function () {
                    v.leave_room()
                    v.rooms = []
                    v.socket = null
                    v.state = 'login'
                    M.toast({
                        html : '<span>Server disconnected!</span>'
                    })
                })
            }
        },
        on_room_create(name) {
            if (name.trim() == '') {
                M.toast({
                    html : '<span>Please enter a name!</span>'
                })
            } else {
                this.state = 'loading'
                this.socket.emit(CREATE_ROOM, {
                    name,
                    owner_name : this.user_name
                })
            }
        },
        on_room_selected(name) {
            this.state = 'loading'
            this.socket.emit(JOIN_ROOM, {
                name
            })
        }
    },
    mounted : function() {
        this.request_devices_perms()
    }
});